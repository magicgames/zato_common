from lxml import etree


def credential(root, user, password):
    credentials = etree.SubElement(root, "Credentials")
    etree.SubElement(credentials, 'User').text = user
    etree.SubElement(credentials, 'Password').text = password

def ocupancy(node,adult_count,child_count,num_elem,age,sub_node="Occupancy"):
    occupation_room = etree.SubElement(node, sub_node)
    etree.SubElement(occupation_room, "AdultCount").text = str(adult_count)
    etree.SubElement(occupation_room, "ChildCount").text = str(child_count)

    if int(child_count) > 0:
        guest_list = etree.SubElement(occupation_room, "GuestList")
        for x in xrange(num_elem):
            for child in age:
                customer = etree.SubElement(guest_list, "Customer", type="CH")
                etree.SubElement(customer, "Age").text = str(child)


def guest_occupation(occupation_list, **kwargs):

    kwargs = kwargs['kwargs']
    print kwargs
    try:
        adult_count = kwargs['adultCount']
    except KeyError as e:
        adult_count = 0

    try:
        child_count = kwargs['childCount']
    except KeyError as e:
        child_count = 0

    try:
        num_room = kwargs['numRoom']

    except KeyError as e:
        num_room = 0
    age = kwargs.get('age')
    occupation = etree.SubElement(occupation_list, "HotelOccupancy")
    etree.SubElement(occupation, "RoomCount").text = str(num_room)
    ocupancy(occupation,adult_count,child_count,num_room,age)


def default_options(root, lang, destination, num_items=999):
    etree.SubElement(root, "Language").text = lang
    etree.SubElement(root, "PaginationData", pageNumber="1", itemsPerPage=str(num_items))

    if 'destinationCode' in destination:
        try:
            type_destination = destination['type']
        except KeyError:
            type_destination = "SIMPLE"

        dest_ele = etree.SubElement(root, "Destination", code=destination['destinationCode'], type=type_destination)
        if 'zoneCode' in destination:
            zone = etree.SubElement(dest_ele, 'ZoneList')
            etree.SubElement(zone, 'Zone', type=type_destination, code=str(destination['zoneCode']))


def search_destination(es, index, query):
    results = []
    try:
        dest = es.search(
            index=index,
            doc_type="destination",
            body={"sort": [
                {"_score": {"order": "desc"}},
                {"num_hotel": {"order": "desc"}},
                "_score"
            ],
                "query": {
                    "match": {
                        "_all": {
                            "query": query
                        }
                    }
                },
                "size": 5
            }
        )
        if int(dest['hits']['total']) > 0:
            destinations = dest['hits']['hits']
            for destination in destinations:
                res = destination['_source']
                id = destination['_id']
                name = res['destination_name']
                code = res['destination_code']

                ht = {
                    'Id': id,
                    'destinationName': name,
                    'destinationCode': code,
                    'numHotel': res['num_hotel'],
                    'terminalCode': res['terminal_code'],
                    'country': res['country'],
                    'searchType': 'Destino'

                }
                results.append(ht)

    except Exception as e:
        print(e)
        pass
    return results


def search_hotel(es,index,query,size=5):
        results = []
        try:
            dest = es.search(
                index=index,
                doc_type="hotel",
                body={"sort": [
                    {"_score": {"order": "desc"}},

                ],
                    "query": {
                        "match": {
                            "_all": {
                                "query": query
                            }
                        }
                    },
                    "size": size
                }
            )
            if int(dest['hits']['total']) > 0:
                hotels = dest['hits']['hits']
                for hotel in hotels:
                    res = hotel['_source']
                    id_hotel = res['hotelCode']['hotelBeds']
                    destination_name = res['destinationName']
                    destination_code = res['destinationCode']
                    zone_name = res['zoneName']
                    zone_code = res['zoneCode']
                    terminal_code = res['terminalCode']
                    hotel_name=res['hotelName']
                    country = res['country']

                    ht = {
                        'hotelId':id_hotel,
                        'destinationName': destination_name,
                        'destinationCode': destination_code,
                        'zoneName': zone_name,
                        'zoneCode': zone_code,
                        'terminalCode':terminal_code,
                        'country': country,
                        'hotelName':hotel_name,
                        'searchType': 'Hotel'

                    }
                    results.append(ht)

        except Exception as e:
            pass
        return results


def search_zone(es, index, query):
    results = []
    try:
        zone = es.search(
            index=index,
            doc_type="zone",
            body={"sort": [
                {"_score": {"order": "desc"}},
                {"num_hotel": {"order": "desc"}},
            ],
                "query": {
                    "match": {
                        "_all": {
                            "query": query
                        }
                    }
                },
                "size": 5
            }
        )
        if int(zone['hits']['total']) > 0:
            zones = zone['hits']['hits']
            for zone in zones:
                res = zone['_source']
                destination_name = res['destination_name']
                destination_code = res['destination_code']
                zone_name = res['zone_name']
                zone_code = res['zone_code']
                num_hotels = res['num_hotel']
                terminal_code = res['terminal_code']
                country = res['country']

                ht = {
                    'destinationName': destination_name,
                    'destinationCode': destination_code,
                    'zoneName': zone_name,
                    'zoneCode': zone_code,
                    'numHotel': num_hotels,
                    'terminalCode':terminal_code,
                    'country': country,
                    'searchType': 'Zone'

                }
                results.append(ht)

    except Exception as e:
        print(e)
        pass
    return results


def simulate_zone(es, index, query):
    results = []
    try:
        zone = es.search(
            index=index,
            doc_type="zone",
            body={"sort": [
                {"_score": {"order": "desc"}},
                {"num_hotel": {"order": "desc"}},
            ],
                "query": {
                    "match": {
                        "_all": {
                            "query": query
                        }
                    }
                },
                "size": 20
            }
        )
        if int(zone['hits']['total']) > 0:
            zones = zone['hits']['hits']
            for zone in zones:
                res = zone['_source']
                destination_name = res['destination_name']
                destination_code = res['destination_code']
                zone_name = res['zone_name']
                zone_code = res['zone_code']
                num_hotels = res['num_hotel']
                terminal_code = res['terminal_code']
                country = res['country']

                ht = {
                    'code': destination_code,
                    'name': country + ' | ' + destination_name + ' | ' + zone_name,
                    'terminal_code': terminal_code
                }
                results.append(ht)

    except Exception as e:
        print(e)
        pass
    return results


def simulate_destination(es, index, query):
    results = []
    try:
        dest = es.search(
            index=index,
            doc_type="destination",
            body={"sort": [
                {"_score": {"order": "desc"}},
                {"num_hotel": {"order": "desc"}},
                "_score"
            ],
                "query": {
                    "match": {
                        "_all": {
                            "query": query
                        }
                    }
                },
                "size": 20
            }
        )
        if int(dest['hits']['total']) > 0:
            zones = dest['hits']['hits']
            for zone in zones:
                res = zone['_source']
                destination_name = res['destination_name']
                destination_code = res['destination_code']
                terminal_code = res['terminal_code']
                country = res['country']

                ht = {
                    'code': destination_code,
                    'name': country + ' | ' + destination_name +' | Todos los destinos',
                    'terminal_code': terminal_code
                }
                results.append(ht)

    except Exception as e:
        print(e)
        pass
    return results



