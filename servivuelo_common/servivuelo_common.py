# -*- coding: utf-8 -*-


def generate_common(params,conn):
        people = params['people']
        destinations = params['destinations']
        include_low_cost = params['lowcost']
        direct_flight = params['direct']
        num_adult = params['numAdult']
        num_child = params['numChild']
        num_infant = params['numInfant']
        num_adult_resident = params['numAdultResident']
        num_child_resident = params['numChildResident']
        num_infant_resident = params['numInfantResident']
        class_pref = None
        res=None

        with conn.client() as client:
            credential = generate_credential(client, params)
            travelers = generate_travelers(client, people)
            flight_segments = generate_segments(client, destinations)

            search = AirAvailRQ(client).create(direct_fligts=direct_flight,
                                               include_lowcost=include_low_cost,
                                               class_pref=class_pref,
                                               travelers=travelers,
                                               flight_segments=flight_segments)
        return credential,search,num_adult,num_child,num_infant,num_adult_resident,num_child_resident,num_infant_resident


def generate_credential(client,d):
    credential = client.factory.create('AuthenticationCredentials')
    credential.AgencyID = d['id_agencia']
    credential.UserID = d['username']
    credential.Password = d['password']
    return credential


def generate_travelers(client,people):
    traveler_info = []
    for person in people:
        passenger = AirTravelerInfo(client).create(traveler_type=person['type'],
                                                   is_resident=person['is_resident'])
        traveler_info.append(passenger)
    return ArrayOfAirTravelerInfo(client).create(traveler_info=traveler_info)


def generate_segments(client,destinations):
    segments = []
    for destination in destinations:
        segment = AirFlightSegmentRQ(client).create(destination['departure'],
                                                    destination['arrive'],
                                                    destination['date'],
                                                    destination['time'])
        segments.append(segment)
    return ArrayOfAirFlightSegmentRQ(client).create(segments)


class BaseObject(object):
    def __init__(self, client):
        self.client = client


class AuthenticationCredentials(BaseObject):
    def create(self, agency_id, user_id, password):
        credential = self.client.factory.create('AuthenticationCredentials')
        credential.AgencyID = agency_id
        credential.UserID = user_id
        credential.Password = password
        return credential


## TRAVELERS

class AirAvailClassPreferences(BaseObject):
    def create(self):
        credential = self.client.factory.create('AirAvailClassPreferences')
        return credential


class AirTravelerTypes(BaseObject):
    def create(self):
        credential = self.client.factory.create('AirTravelerTypes')
        return credential


class AirTravelerInfo(BaseObject):
    def create(self, traveler_type, is_resident):
        credential = self.client.factory.create('AirTravelerInfo')
        credential.TravelerType = traveler_type
        credential.IsResident = is_resident
        return credential


class ArrayOfAirTravelerInfo(BaseObject):
    def create(self, traveler_info):
        credential = self.client.factory.create('ArrayOfAirTravelerInfo')
        credential.AirTravelerInfo = traveler_info
        return credential


## AIR FLIGHT

class AirFlightSegmentRQ(BaseObject):
    def create(self, departure_location, arrive_location, departure_date, departure_time):
        credential = self.client.factory.create('AirFlightSegmentRQ')
        credential.DepartureAirportLocationCode = departure_location
        credential.ArrivalAirportLocationCode = arrive_location
        credential.DepartureDate = departure_date
        credential.DepartureTime = departure_time
        return credential


class ArrayOfAirFlightSegmentRQ(BaseObject):
    def create(self, flight_segment):
        credential = self.client.factory.create('ArrayOfAirFlightSegmentRQ')
        credential.AirFlightSegmentRQ = flight_segment
        return credential


### SEARCH

class AirAvailRQ(BaseObject):
    def create(self, direct_fligts, include_lowcost, class_pref, travelers, flight_segments):
        credential = self.client.factory.create('AirAvailRQ')
        credential.DirectFlightsOnly = direct_fligts
        credential.IncludeLowCost = include_lowcost
        credential.ClassPref = class_pref
        credential.Travelers = travelers
        credential.FlightSegments = flight_segments
        return credential


## RATES
class AirAvailRateNotesRQ(BaseObject):
    def create(self, token, pricing_group_id, itineraries_id, adult_number=0, child_number=0, infant_number=0):
        credential = self.client.factory.create('AirAvailRateNotesRQ')
        credential.AvailRequestID = token
        credential.PricingGroupID = pricing_group_id
        for it_int in itineraries_id:
            credential.ItinerariesID.int.append(it_int)

        credential.AdultNumber = adult_number
        credential.ChildrenNumber = child_number
        credential.InfantNumber = infant_number
        return credential


#### BOOKING PROCESS

class AirTravelerTSAData(BaseObject):
    def create(self,
               first_name,
               last_name,
               gender,
               birth_date,
               doc_exp_date,
               doc_issue_country,
               doc_type,
               doc_number,
               birth_country,
               nationality_country,
               visa_issued_city=None, visa_number=None, visa_issue_country=None, visa_issue_date=None,
               is_usa_resident=None, usa_city=None, usa_state=None, usa_residence_type=None, usa_address=None, usa_zipcode=None):



        credential = self.client.factory.create('AirTravelerTSAData')
        credential.FirstName = first_name
        credential.LastName = last_name
        """
        Gender
        M: Male,
        F: Female
        MI: Male Infant
        FI: Female Infant
        U: Undisclosed Gender

        """

        credential.Gender = gender  #
        credential.BirthDate = birth_date  # formato dd/mm/yyyy
        credential.DocumentExpirationDate = doc_exp_date
        credential.DocumentIssueCountry = doc_issue_country

        """
        DocumentType
        I: Documento Nacional de Identidad,
        P: Pasaporte,
        IP: Tarjeta de residente o tarjeta pasaporte,
        A: Otra tarjeta de identidad,
        AC: Certificado de miembro de tripulacion,
        F: Cartilla verde americana

        """
        credential.DocumentType = "P"
        credential.DocumentNumber = doc_number
        credential.BirthCountry = birth_country
        credential.NationalityCountry = nationality_country

        if visa_number is not None:
            credential.VisaIssueCity = visa_issued_city
            credential.VisaNumber = visa_number
            credential.VisaIssueCountry = visa_issue_country
            credential.VisaIssueDate = visa_issue_date  # formato dd/mm/yyyy
            credential.IsResidentUSA = is_usa_resident
            credential.USA_City = usa_city
            credential.USA_State = usa_state
            credential.USA_ResidenceType = usa_residence_type  # (D: en el destino, R: residencia)
            credential.USA_Address = usa_address
            credential.USA_ZipCode = usa_zipcode
        else:
            credential.VisaIssueCity = ""
            credential.VisaNumber = ""
            credential.VisaIssueCountry = ""
            credential.IsResidentUSA = False
            credential.USA_City = ""
            credential.USA_State = ""
            credential.USA_ResidenceType = "D"  # (D: en el destino, R: residencia)
            credential.USA_Address = ""
            credential.USA_ZipCode = ""



        return credential


class AirTraveler(BaseObject):
    def create(self, traveler_obj):
        flight_to_usa=traveler_obj.get('flightToUsa', False)

        traveler_type = traveler_obj.get('travelerType')
        additional_baggages = traveler_obj.get('additionalBaggages', "0")
        traveler_title = traveler_obj.get('travelerTitle')
        document_type = traveler_obj.get('documentType')
        traveler_id = traveler_obj.get('travelerId')
        infant_id = traveler_obj.get('infantId')
        first_name = traveler_obj.get('firstName')
        last_name = traveler_obj.get('lastName')
        document_number = traveler_obj.get('documentNumber')
        email = traveler_obj.get('email')
        phone = traveler_obj.get('phone')
        birth_date = traveler_obj.get('birthDate')
        is_resident = traveler_obj.get('isResident')
        flyer_program = traveler_obj.get('frequentFlyerProgram')
        flyer_card_number = traveler_obj.get('frequentFlyerCardNumber')
        if is_resident is not None:
            resident_document_type = traveler_obj.get('residentDocumentType')
            resident_city_code = traveler_obj.get('residentCityCode')
            resident_certificate_number = traveler_obj.get('residentCertificateNumber')

           
        if flight_to_usa:
            tsdata=traveler_obj['usa']
            gender = tsdata['gender']
            doc_exp_date = tsdata['expire_date']
            doc_issue_country = tsdata['expedicion']
            doc_type = traveler_obj.get('documentType')
            doc_number = traveler_obj.get('documentNumber')
            birth_country = tsdata['nationality']
            nationality_country = tsdata['nationality']

            tsdata_data = AirTravelerTSAData(self.client).create(first_name=first_name,
                                                      last_name=last_name,
                                                      gender=gender,
                                                      birth_date=birth_date,
                                                      doc_exp_date=doc_exp_date,
                                                      doc_issue_country=doc_issue_country,
                                                      doc_type=doc_type,
                                                      doc_number=doc_number,
                                                      birth_country=birth_country,
                                                      nationality_country=nationality_country)

        else:
            tsdata_data = None

        credential = self.client.factory.create('AirTraveler')
        credential.TravelerType = traveler_type  # AirTravelerTypes ( Adulto, niño , bebe)
        credential.TravelerTitle = traveler_title  # AirTravelerTitles(Mr, Mrs, Ms)
        credential.DocumentType = document_type  # Document Type(DNI, NIE, PAS, OTR)
        credential.TravelerID = traveler_id  # TravelerID
        credential.InfantID = infant_id  # TravelerID of infant assigned to adult
        credential.FirstName = first_name
        credential.LastName = last_name
        credential.DocumentNumber = document_number
        credential.Email = email
        credential.Phone = phone
        credential.BirthDate = birth_date
        credential.FrequentFlyerProgram = flyer_program
        credential.FrequentFlyerCardNumber = flyer_card_number
        credential.AdditionalBaggages = additional_baggages
        credential.IsResident = is_resident
        credential.ResidentDocumentType = resident_document_type  # ResidentDocumentType(DN,CR,GR,TR,MR,AM)
        credential.ResidentCityCode = resident_city_code
        credential.ResidentCertificateNumber = resident_certificate_number
        credential.TSAData = tsdata_data  # TSAData
        return credential


class AirBookingRQ(BaseObject):
    def create(self, token, price_group_id, itineraries_id, travelers, issuer_notes, customer_notes,
               cancellation_insurance):
        credential = self.client.factory.create('AirBookingRQ')
        credential.AvailRequestID = token
        credential.PricingGroupID = price_group_id
        for it_int in itineraries_id:
            credential.ItinerariesID.int.append(it_int)

        for traveler in travelers:
            credential.AirTravelers.AirTraveler.append(traveler)

        credential.IssuerNotes = issuer_notes
        credential.CustomerNotes = customer_notes
        credential.CancellationInsurance = cancellation_insurance

        return credential


class AirBookingImportRQ(BaseObject):
    def create(self, agency_id, user_id, locator):
        credential = self.client.factory.create('AirBookingImportRQ')
        credential.AgencyID = agency_id
        credential.UserID = user_id
        credential.Locator = locator

        return credential


class AirBookingStatus(BaseObject):
    def create(self):
        credential = self.client.factory.create('AirBookingStatus')
        return credential


class AirBookingMarkToTicketingRQ(BaseObject):
    def create(self, booking_id, payment_form, card_id, insurance_amount=0, observations='', amount_difference=0):
        credential = self.client.factory.create('AirBookingMarkToTicketingRQ')
        credential.BookingID = booking_id
        credential.PaymentForm = payment_form
        credential.CardID = card_id
        credential.InsuranceAmount = insurance_amount
        credential.Observations = observations
        credential.AmountDifference = amount_difference

        return credential


class AirBookingCancelRQ(BaseObject):
    def create(self, booking_id):
        credential = self.client.factory.create('AirBookingCancelRQ')
        credential.BookingID = booking_id

        return credential


class AirBookingTicketingRQ(BaseObject):
    def create(self, booking_id, card_id, insurance_amount=0, amount_difference=0):
        credential = self.client.factory.create('AirBookingTicketingRQ')
        credential.BookingID = booking_id
        credential.CardID = card_id
        credential.InsuranceAmount = insurance_amount
        credential.AmountDifference = amount_difference

        return credential


### INVOICES

class AirInvoiceAvsisRQ(BaseObject):
    def create(self, agency_id, user_id, password, localizador):
        credential = self.client.factory.create('AirInvoiceAvsisRQ')
        credential.IDAgencia = agency_id
        credential.UserID = user_id
        credential.Password = password
        credential.Localizador = localizador
        return credential
