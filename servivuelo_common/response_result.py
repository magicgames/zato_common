# -*- coding: utf-8 -*-
__author__ = 'ballbrk'


def response_search(result,num_adult=0, num_child=0,num_infant=0,num_adult_resident=0,num_child_resident=0,num_infant_resident=0):
        """
        Procesa el resultado para poder obtener los vuelos
        :param result:
        :return:
        """
        itinerarios = {}
        error = []

        if 'Error' in result:
            if result.Error.Code is None:
                for warning in result.AirAvail.Warnings.Warning:
                    x = {
                        'code': warning['WarningCode'],
                        'description': warning['Description']

                    }
                    error.append(x)
            else:
                x = {
                    'code': result.Error.Code,
                    'description': result.Error.Description

                }
                error.append(x)
            return {'status': 'KO', 'error': error}

        if result.AirAvail.AirItineraries is None:
            x = {
                'code': 'NO FLIGHT',
                'description': "No hay vuelos para esas fechas"

            }
            error.append(x)
            return {'Status': 'KO', 'Error': error}

        for itinerary in result.AirAvail.AirItineraries.AirItinerary:
            scale = None
            if len(itinerary.AirItineraryLegs.AirItineraryLeg) > 1:
                scale = []
                for scale_obj in itinerary.AirItineraryLegs.AirItineraryLeg:
                    x = {
                        'departure': scale_obj['DepartureAirportLocationCode'],
                        'departureTime': scale_obj['DepartureDateTime'],
                        'arrive': scale_obj['ArrivalAirportLocationCode'],
                        'arriveTime': scale_obj['ArrivalDateTime'],
                        'flightNumber': scale_obj['FlightNumber'],
                        'operationCarrier': scale_obj['OperatingCarrierCode'],
                        'marketingCarrier': scale_obj['MarketingCarrierCode']
                    }
                    scale.append(x)

            x = {

                itinerary['ItineraryID']: {
                    'id': itinerary['ItineraryID'],
                    'departure': itinerary['DepartureAirportLocationCode'],
                    'departureTime': itinerary['DepartureDateTime'],
                    'arrive': itinerary['ArrivalAirportLocationCode'],
                    'arriveTime': itinerary['ArrivalDateTime'],
                    'flightNumber': itinerary.AirItineraryLegs.AirItineraryLeg[0]['FlightNumber'],
                    'operationCarrier': itinerary.AirItineraryLegs.AirItineraryLeg[0]['OperatingCarrierCode'],
                    'marketingCarrier': itinerary.AirItineraryLegs.AirItineraryLeg[0]['MarketingCarrierCode'],
                    'scale': scale

                }

            }
            itinerarios.update(x)
        vuelos = []

        for air_pricing_group in result.AirAvail.AirPricingGroups.AirPricingGroup:

            discount = float(air_pricing_group['DiscountAmount'])
            price_adult = float(air_pricing_group['AdultTicketAmount'])
            price_child = float(air_pricing_group['ChildrenTicketAmount'])
            price_infant = float(air_pricing_group['InfantTicketAmount'])
            tax_adult = float(air_pricing_group['AdultTaxAmount'])
            tax_child = float(air_pricing_group['ChildrenTaxAmount'])
            tax_infant = float(air_pricing_group['InfantTaxAmount'])
            aramix_fee = float(air_pricing_group['AramixFeeAmount'])
            price_per_user = {

                'adult': {
                    'price': price_adult,
                    'tax': tax_adult,
                    'fee': aramix_fee,
                    'discount': discount

                },
                'child': {
                    'price': price_child,
                    'tax': tax_child,
                    'fee': aramix_fee,
                    'discount': discount

                },
                'infant': {
                    'price': price_infant,
                    'tax': price_infant,
                    'fee': aramix_fee,
                    'discount': discount

                }

            }
            price_total_adult = ((price_adult * num_adult) - (price_adult * num_adult_resident * discount)) + (
                (tax_adult + aramix_fee) * num_adult)
            price_total_child = ((price_child * num_child) - (price_child * num_child_resident * discount)) + (
                (tax_child + aramix_fee) * num_child)
            price_total_infant = ((price_infant * num_infant) - (price_infant * num_infant_resident * discount)) + (
                (tax_infant + aramix_fee) * num_infant)

            price = price_total_adult + price_total_child + price_total_infant

            price_group = air_pricing_group['GroupID']
            is_tour_operation = air_pricing_group['IsTourOperator']
            is_lowcost = air_pricing_group['IsLowCost']

            fare_notes = []
            for fare_note in air_pricing_group['FareNotes'].AirFareNote:
                x = {

                    'code': fare_note.NoteCode,
                    'category': fare_note.Category,
                    'description': fare_note.Description,

                }

                fare_notes.append(x)

            iti = {}
            for group_options in air_pricing_group.AirPricingGroupOptions.AirPricingGroupOption:
                for it in group_options.AirPricedItineraries.AirPricedItinerary:

                    fare_code = it.AirPricedItineraryLegs.AirPricedItineraryLeg[0]['FareCode']
                    fare_type = it.AirPricedItineraryLegs.AirPricedItineraryLeg[0]['FareType']
                    num_seats = it.AirPricedItineraryLegs.AirPricedItineraryLeg[0]['AvailableSeats']
                    num_bagage = it.AirPricedItineraryLegs.AirPricedItineraryLeg[0]['QuantityBaggageADT']
                    weight_bagage = it.AirPricedItineraryLegs.AirPricedItineraryLeg[0]['QuantityWeightADT']
                    type_bagage = it.AirPricedItineraryLegs.AirPricedItineraryLeg[0]['QuantityTypeADT']
                    measure_bagage = it.AirPricedItineraryLegs.AirPricedItineraryLeg[0]['MeasureUnitADT']
                    cabin_type = it.AirPricedItineraryLegs.AirPricedItineraryLeg[0]['CabinType']
                    x = {
                        it['ItineraryID']: {
                            'fareCode': fare_code,
                            'fareType': fare_type,
                            'freeSeat': num_seats,
                            'numBagage': num_bagage,
                            'weightBagage': weight_bagage,
                            'typeBagage': type_bagage,
                            'measureBagage': measure_bagage,
                            'cabinType': cabin_type
                        }

                    }
                    segment = int(str(it['ItineraryID'])[0])
                    if segment not in iti:
                        iti[segment] = {}

                    iti[segment].update(x)
            x = {
                'price': price,
                'pricePerUser': price_per_user,
                'priceGroup': price_group,
                'isTourOperation': is_tour_operation,
                'isLowcost': is_lowcost,
                'fareNotes': fare_notes,

                'flightList': iti

            }

            vuelos.append(x)

        for vuelo in vuelos:

            for k in vuelo['flightList'].keys():
                # keys solo tiene los segmentos ( 1..n)
                for v in vuelo['flightList'][k].keys():
                    vuelo['flightList'][k][v].update(itinerarios[v])
        res = {'status': 'OK', 'response': vuelos,'totalItem':len(vuelos),'token': result.RequestID}

        return res
